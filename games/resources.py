from tastypie import fields
from tastypie.resources import ModelResource
from gameserver.games.models import Game, Turn, State, Action, TurnIndex

class StateResource(ModelResource):
    class Meta:
        queryset = State.objects.all()
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get', 'post']
        resource_name = 'states'
        fields = ['info',]
        include_resource_uri = False

class TurnResource(ModelResource):
    initial_state = fields.ToOneField('gameserver.games.resources.StateResource', 'initial_state', full = True)
    action = fields.ToOneField('gameserver.games.resources.ActionResource', 'action', full = True)
    final_state = fields.ToOneField('gameserver.games.resources.StateResource', 'final_state', full = True)
    class Meta:
        queryset = Turn.objects.all()
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get', 'post',]
        resource_name = 'turns'
        fields = ['id', 'initial_state', 'action', 'final_state',]
        include_resource_uri = False

class TurnIndexResource(ModelResource):
    turn = fields.ToOneField('gameserver.games.resources.TurnResource', 'turn', full = True)
    class Meta:
        queryset= TurnIndex.objects.all()
        fields = ['turn', 'index',]
        include_resource_uri = False


class GameResource(ModelResource):
    #turns = fields.ToManyField('gameserver.games.resources.TurnResource', 'turns', full = True)
    turns = fields.ToManyField('gameserver.games.resources.TurnIndexResource', attribute=lambda bundle: bundle.obj.turns.through.objects.filter(game=bundle.obj) or bundle.obj.turns, full=True)
    
    class Meta:
        queryset = Game.objects.all()
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get', 'post',]
        resource_name = 'games'

class ActionResource(ModelResource):
    class Meta:
        queryset = Action.objects.all()
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get', 'post',]
        resource_name = 'actions'
        fields = ['info',]
        include_resource_uri = False

