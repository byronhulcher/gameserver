from django.contrib import admin
from gameserver.games.models import Game, Turn, State, Action

class TurnAdmin(admin.ModelAdmin):
    model = Turn
    readonly_fields = ['id',]
    ordering = ['created_on']
    list_display = ('id','initial_state', 'action', 'final_state')

class TurnInline(admin.TabularInline):
    model = Game.turns.through
    readonly_fields = ['id',]

class GameAdmin(admin.ModelAdmin):
    model = Game
    readonly_fields = ['id',]
    inlines = [
        TurnInline
    ]
    ordering = ['created_on']

class StateAdmin(admin.ModelAdmin):
    model = State
    readonly_fields = ['id',]
    ordering = ['created_on']

class ActionAdmin(admin.ModelAdmin):
    model = Action
    readonly_fields = ['id',]
    ordering = ['created_on']
    list_display = ('id','get_action')
    list_display_links = list_display
    
    def get_action(self, obj):
      return ("%s" % (obj.info['action']))
    get_action.short_description = 'Action'

admin.site.register(Game, GameAdmin)
admin.site.register(Turn, TurnAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(Action, ActionAdmin)