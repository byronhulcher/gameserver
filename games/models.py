from django.db import models
from jsonfield import JSONField

class Game(models.Model):
    created_on = models.DateTimeField(auto_now_add = True)
    modified_on = models.DateTimeField(auto_now = True)
    turns = models.ManyToManyField('games.Turn', through = 'TurnIndex')
    def __unicode__(self):
        return "Game %s" % (self.id,)

class Turn(models.Model):
    created_on = models.DateTimeField(auto_now_add = True)
    modified_on = models.DateTimeField(auto_now = True)
    
    initial_state = models.ForeignKey('games.State', related_name = '+')
    action = models.ForeignKey('games.Action', related_name = '+')
    final_state = models.ForeignKey('games.State', related_name = '+')

    class Meta:
        unique_together = ('initial_state', 'action', 'final_state',)

    def __unicode__(self):
        return "Turn %s" % (self.id,)

class State(models.Model):
    created_on = models.DateTimeField(auto_now_add = True)
    modified_on = models.DateTimeField(auto_now = True)
    info = JSONField(null = True, blank = True)

    def __unicode__(self):
        return "State %s" % (self.id)

class Action(models.Model):
    created_on = models.DateTimeField(auto_now_add = True)
    modified_on = models.DateTimeField(auto_now = True)
    info = JSONField(null = True, blank = True)

    def __unicode__(self):
        return "Action %s" % (self.id)

class TurnIndex(models.Model):
    created_on = models.DateTimeField(auto_now_add = True)
    modified_on = models.DateTimeField(auto_now = True)
    game = models.ForeignKey('games.Game')
    turn = models.ForeignKey('games.Turn')
    index = models.IntegerField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.index = len(self.game.turns.all()) + 1
        super(TurnIndex, self).save(*args, **kwargs)