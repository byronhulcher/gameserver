from django.conf.urls import patterns, include, url
from tastypie.api import Api
from gameserver.games.resources import GameResource, TurnResource, StateResource, ActionResource
from gameserver.sites.api.resources import UserResource


# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(GameResource())
v1_api.register(TurnResource())
v1_api.register(StateResource())
v1_api.register(ActionResource())
v1_api.register(UserResource())

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
)